// create schema

const mongoose = require("mongoose");
const eventSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 2
    },
    cost: {
        type: Number,
        required: true,
        min: 1
    },
    category: {
        type: String,
        required: true,
        enum: ["business", "casual", "party", "general"],
        default: "general"
    },
    imageUrl: String,
    owner: String,
    location: String

});

module.exports = mongoose.model('Event', eventSchema);