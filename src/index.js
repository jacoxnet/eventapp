// package express
const express = require('express');
const app = express();

// environment variables
require('dotenv').config();
const port = process.env.PORT;

// setup database
const dbSetup = require('./database/setup');

// require routes
const eventRoutes = require('./routes/eroutes');
const authRoutes = require('./routes/authRoutes');

// Seeders
const {seedAdmin} = require('./seeders/admin');
seedAdmin();

// setup express
app.use(express.json());

// setup db including mongoose
dbSetup();

// identify routes
app.use(eventRoutes);
app.use('/auth', authRoutes);

app.listen(port, () => console.log(`app listening on port ${port}`));