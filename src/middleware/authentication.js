// jwt service function
const {verifyToken} = require('../services/jwtservice');

// parse provided token in header to retrieve identifying user info
exports.authenticateUser = async (req, res, next) => {
    if (!req.headers.authorization) return res.status(401).json({message: 'authorization header required'});
    try {
        let aftersplit = req.headers.authorization.split(' ');
        if (aftersplit[0] !== 'Bearer') return res.json({message: "authorization format is 'Bearer <token>'"});
        let token = aftersplit[1];
        let decodedToken = await verifyToken(token);
        if (!decodedToken) return res.status(401).json({message: 'invalid authorization token: please login'});
        // select out our fields and save in req.user so other parts of app can identify user
        req.user = (({name, email, role, password}) => {
            return {name, email, role, password};
        })(decodedToken);
        next();
    } catch (err) {
        return res.status(500).json({err});
    }
};

exports.checkIfAdmin = (req, res, next) => {
    if (req.user.role != 'admin') return res.status(401).json({
        message: 'this route is restricted to admin users'
    });
    return next();
};