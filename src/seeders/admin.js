// require user model
const User = require('../models/user');

// require bcrypt services
const {makeHash} = require('../services/bcryptservice');

// Default fields for new admin account 
const DEFAULTADMIN = {
    name: "Annie Administrator",
    email: "admin@event.com",
    role: "admin",
    password: ""
};

// Default password for admin account
ADMINPASS = 'mypass';

// add admin account if not one already
exports.seedAdmin = async () => {
    try {
        let admin = await User.findOne({role: 'admin'});
        if (admin) return 'admin account already exists';
        let hash = await makeHash(ADMINPASS);
        DEFAULTADMIN.password = hash;
        await User.create(DEFAULTADMIN);
        console.log('admin account created');
    } catch (err) {
        throw err;
    }
};
