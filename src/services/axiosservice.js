const axios = require('axios');
const imgBaseUrl = 'https://imagegen.herokuapp.com/?category=';

exports.getImage = (category) => {
    return new Promise((resolve, reject) => {
        axios.get(imgBaseUrl + category)
            .then ((response) => {
                resolve(response.data.image);
            })
            .catch ((err) => {
                reject(err);
            });
    });
};

