// jwtoken stuff
const jwt = require('jsonwebtoken');
const SECRET = process.env.SECRET;
const EXPIRY = process.env.EXPIRY;

// jwt service function
// argument: object with properties of new user
// returns: successful promise with signed token
exports.mySign = (payload) => {
    // select our key fields (name, email, role) for payload calculation
    // NOT including password
    selectedPayload = (({name, email, role}) => {
        return {name, email, role};
    })(payload);
    // use jwt to sign selected payload and return token
    return new Promise((resolve, reject) => {
        jwt.sign(selectedPayload, SECRET, {expiresIn: EXPIRY}, (err, token) => {
            if (!err) resolve(token);
            reject(err);
        });
    });
};

// jwt service function
// argument is token, returns successful promise with decoded token
exports.verifyToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, SECRET, (err, decodedToken) => {
            if (!err) resolve(decodedToken);
            reject(err);
        });
    });
};
