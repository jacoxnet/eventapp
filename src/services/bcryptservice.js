// bcrypt and constants
const bcrypt = require('bcrypt');
const SALTROUNDS = 10;

exports.makeHash = (plainText) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(plainText, SALTROUNDS, (err, hash) => {
            if (!err) resolve(hash);
            reject(err);
        });
    });
};

exports.compareHash = (providedPass, hash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(providedPass, hash, (err, result) => {
            if (!err) resolve(result);
            reject(err);
        });
    });
};
