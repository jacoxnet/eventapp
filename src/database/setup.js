// setup connection to atlas cloud db

const mongoose = require('mongoose');

const url = process.env.DB_URL;

const connectionParams = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
};

module.exports = function () {
    mongoose.connect(url, connectionParams)
        .then(() => {
            console.log('Connected to database ');
        })
        .catch((err) => {
            console.error(`Error connecting to the database. \n${err}`);
        });
};