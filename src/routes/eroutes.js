const express = require('express');
const router = express.Router();
const eventctrl = require('../controllers/econtrollers');
const {authenticateUser, checkIfAdmin} = require('../middleware/authentication');

router.post('/events', authenticateUser, eventctrl.createNewEvent);

router.get('/events', authenticateUser, eventctrl.fetchAllEvents);

router.get('/events/:id', authenticateUser, eventctrl.fetchSingleEvent);

router.put('/events/:id', authenticateUser, checkIfAdmin, eventctrl.updateSingleEvent);

router.delete('/events/:id', authenticateUser, checkIfAdmin, eventctrl.deleteSingleEvent);

module.exports = router;