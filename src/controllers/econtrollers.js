// Functions used in routes

const Event = require('../models/emodel');

// require image service function
const {getImage} = require('../services/axiosservice');

// create a new event
exports.createNewEvent = async (req, res) => {
    // retrieve new event details from req.body
    try {
        const event = req.body;
        newEvent = await Event.create(event);
        newEvent.imageUrl = await getImage(newEvent.category);
        await newEvent.save();
        return res.status(200).json({
            message: 'new event created',
            newEvent
        });
    } catch (err) {
        return res.status(500).json({
            message: err
        });
    }
};

// fetch all events
exports.fetchAllEvents = (req, res) => {
    let conditions = {};
    if (req.query.category) {
        conditions.category = req.query.category;
    }
    if (req.query.author) {
        conditions.author = req.query.author;
    }
    Event.find(conditions, (err, events) => {
        if (err) return res.status(500).json({
            message: err
        });
        return res.status(200).json({
            events
        });
    });
};

// fetch a single event by id
exports.fetchSingleEvent = (req, res) => {
    Event.findById(req.params.id, (err, event) => {
        if (err) return res.status(500).json({
            message: err
        });
        else if (!event) return res.status(404).json({
            message: 'event not found'
        });
        return res.status(200).json({
            event
        });
    });
};

// update a single event by id
exports.updateSingleEvent = async (req, res) => {
    try {
        let newProps = req.body;
        // get new event by specifying option {new: true}
        let newEvent = await Event.findByIdAndUpdate(req.params.id, newProps, {new: true});
        // if we can't locate old event
        if (!newEvent) return res.status(404).json({
            message: 'event not found'
        });
        // update image if new category specified
        if (newProps.category) {
            newEvent.imageUrl = await getImage(newEvent.category);
        }
        await newEvent.save();
        return res.status(200).json({
            message: 'event updated successfully'
        });
    } catch (err) {
        return res.status(500).json({
            message: err
        });
    }
};

// delete a single event by id
exports.deleteSingleEvent = (req, res) => {
    Event.findByIdAndDelete(req.params.id, (err, event) => {
        if (err) return res.status(500).json({
            message: err
        });
        else if (!event) return res.status(404).json({
            message: 'event not found'
        });
        return res.status(200).json({
            message: 'event deleted successfully'
        });
    });
};