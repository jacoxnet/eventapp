// Functions used in authorization

// user model
const User = require('../models/user');

// bcrypt service functions
const {makeHash, compareHash} = require('../services/bcryptservice');

// jwt service function
const {mySign} = require('../services/jwtservice');

// register a new user
exports.registerNewUser = async (req, res) => {
    // make sure required fields completed
    if (!req.body.email || !req.body.password)
    return res.status(400).json({
        message: 'email, password required'
    });
    try {
        // see if email already in db
        let existinguser = await User.findOne({
            email: req.body.email
        });
        if (existinguser) return res.status(400).json({
            message: 'email already exists in system'
        });
        // hash the password
        let hash = await makeHash(req.body.password);
        // create new user in database with password field as the hashed password
        let requestedUser = req.body;
        requestedUser.password = hash;
        let newUser = await User.create(requestedUser);
        // mySign uses jwt to sign token and return
        token = await mySign(newUser);
        return res.status(200).json({
            message: 'registration successful',
            token
        });
    } catch (err) {
        return res.status(500).json({
            message: 'caught err',
            err
        });
    }
};

// allow a user to login
exports.loginUser = async (req, res) => {
    // make sure all fields completed
    if (!req.body.email || !req.body.password)
        return res.status(400).json({
            message: 'email, password required'
        });
    try {
        // see if email already in db
        let retrievedUser = await User.findOne({
            email: req.body.email
        });
        if (!retrievedUser) return res.status(401).json({
            message: 'incorrect email'
        });
        // compare hash of provided password with stored hash
        let compareresult = await compareHash(req.body.password, retrievedUser.password);
        if (!compareresult) return res.status(401).json({
            message: 'Incorrect password'
        });
        // sign payload for token 
        let token = await mySign(retrievedUser);
        return res.status(200).json({
            message: 'login successful',
            token
        });
    } catch (err) {
        return res.status(500).json({
            err
        });
    }
};