# Event Manager Application

version 4

## Updates

1. original
2. added Cloud Version with Authentication/Authorization
3. added dotenv
4. added image url retrieval

- images are retrieved based on category when events are created *or updated*

### Authentication

#### user database

- name
- email
- role (admin or regular)
- password (stored in salted hash)

#### user routes

- signup
  - *POST request to /auth/signup with new user as body*
- login
  - *POST request t0 /auth/login with email and password in body*
  
## Features

Event manager can:

- Create new event
  - *POST request to /events with new event as body*
- Fetch all events
  - *GET request to /events*
- Fetch a single event by id
  - *GET request to /events/:id*
- Update events by id (admin only)
  - *PUT request to /events/:id*
- Delete events by id (admin only)
  - *DELETE request to /events/:id*
- Search for events by category
  - *GET request to /events with parameter `category`

## Event Schema

- title (string)
- cost (number > 0)
- category (enum string, one of business, casual, party, general)
- owner (string)
- location (string)
- imageUrl (from <https://imagegen.herokuapp.com/?category>=)
